'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
  return queryInterface.bulkInsert('user_details',[{
    id:'1',
    user_id:'1',
    first_name:'test',
    last_name:'test Again',
    position : 'manager',
    createdAt:null,
    updatedAt:null
  }])
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('user_details', null,{})
  }
};

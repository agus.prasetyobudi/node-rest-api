'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
   return queryInterface.bulkInsert('users',[{
     id : '1',
     username: 'test',
     password: 'test',
     remember_token : '',
     createdAt: null,
     updatedAt : null
   }])
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('users', null, {})
  }
};

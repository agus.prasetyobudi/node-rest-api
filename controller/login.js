
const {validationResult } = require('express-validator');
const genToken = require('../config/jwt_token')
const bcrypt = require('bcrypt') 
var auth = require('./auth') 
exports.auth = (req,res)=>{ 
    const error = validationResult(req) 
    const data = JSON.parse(JSON.stringify(req.body))
    // Validation Respone
    if(!error.isEmpty()){
        return res.status(400).json({errors: error.array()})
    }  
    console.log('object :', data);
    const saltRounds = bcrypt.genSaltSync(10)
    // let hash_password = bcrypt.hashSync(data.password, saltRounds), 
    let hash_password = data.password
    push_data = {
        "username": data.username
    }
    // Get Data Database
    auth.auth(push_data)
    .then((getData)=>{  
    if(getData != null){
         // Respone Password Validation
         console.log('from Login Controller '+getData)
        if (bcrypt.compareSync(hash_password, getData.password))
        { 
        // Respone Data if True password
            var result = {
            "error" : false,
            "message" : "Data Found",
            "data" : [{
                "token": genToken.generate(getData.id),
                "username" : getData.username,
                "details": getData.user_details
            }]
        }
       res.status(200).json(result) 
    }else{
    //respone if false password
           res.status(400).json({
            "error" : true,
            "message" : "Wrong Password",
            "data" : null
           })
       }
    }else{
        // Respone Fail
        res.status(400).json({
            "error": true,
            "message" : "Username Not Found",
            "data": null
        })
    }
    }) 
}
 
exports.register = (req,res) =>{
    var body = JSON.parse(JSON.stringify(req.body))
    // console.log(body)

    //Salt Sync Bcrypt
    const saltRounds = bcrypt.genSaltSync(10)

    // Formater Data Push
    let request_data = {
       "data_login" :{
        "username" : body.username,
        "password" : bcrypt.hashSync(body.password, saltRounds),
       },
       "data_profile": {
        "first_name" : body.first_name,
        "last_name" : body.last_name
       }
    }
    //Respone From Data
    return res.status(200).json(auth.register(request_data))
}
const db = require('../models/index')
const user = db.user
const detail_user = db.user_details
const {check} = require('express-validator')
exports.auth = (data) =>{ 
    console.log('Data From Login Controlleer :', data);
return user.findOne({
        attributes:['id','username','password' ,'remember_token'],
        where:{
            username:data.username
        },
        include:[{
            model: detail_user,
            attributes:['first_name','last_name','position']
        }]
    }) 
    .then((results)=>{   
        if(results != undefined || results != null){
            return results.dataValues 
        }else{ 
            return null
        }
    }) 
    .catch(err =>{
        console.log('DB Error :', err);
    })
// return data_user 
}
7
exports.register = (data) =>{
  return user.create(data.data_login)
    .then((x)=>{
        var last_id = x.dataValues.id,
        origin = x.dataValues,
        push = {
            "user_id": last_id,
            "first_name":data.data_profile.first_name,
            "last_name" : data.data_profile.last_name,
        }
        detail_user.create(push)
        .then((x)=>{
            // console.log('data :', x );
            return {
                username : origin.username,
                detail_user : [{
                    first_name : x.dataValues.first_name,
                    last_name : x.dataValues.last_name
                }]
            }
        })
        .catch((err)=>{
            throw err
        })
    })
    .catch((err)=>{
        throw(err)
    })
}
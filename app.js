require('dotenv').config();
var express = require('express'),
app = express(), 
logger = require('morgan'),
bodyParser = require('body-parser'),
errorHandler = require('errorhandler'), 
Router = express.Route()
expressValidator = require("express-validator")
port = process.env.NODE_PORT || 8000;

if(process.env.ENV == "development"){
    app.use(errorHandler())
    // console.log('Development Only')
    console.log(process.env.NODE_ENV)
}else{
    console.log(process.env.NODE_ENV)
    // console.log('Production or Live Only')
}

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(express.urlencoded({extended:true}))
// app.use(expressVa//lidator())
app.use((err, req, res, next) => {
    const { start, httpStatus, message, previousError, stack } = err
    console.log(stack);
  
    res.status(httpStatus || 406).json({
      status: false,
      code: httpStatus || 406,
      message,
      data: previousError,
    })
  });
  
  
/////////////// Controller Section ////////////////
const api = require("./routes/api")

////////////// Route Section //////////////////////
// app.post('/api/login', login.auth)
// app.post('/api/register', login.register)
app.use('/api', api)



//////////// Server Section /////////////////////
app.listen(port, ()=>{
    console.log('App listening on port '+port+'!')
})
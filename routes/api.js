const express = require('express')
const Routes = express.Router()
const login = require('../controller/login'),
attd = require('../controller/TransactionFlow'),
jwt = require('../config/jwt_token')
const {body} = require('express-validator')

Routes.post('/login',[
   body('username').not().isEmpty().withMessage('username required'),
   body('password').not().isEmpty()
],login.auth)

Routes.post('/register',[
	body('username').not().isEmpty().withMessage('username required'),
	body('password').not().isEmpty().withMessage('password required'),
	body('first_name').not().isEmpty().withMessage('first name required'),
	body('last_name').not().isEmpty().withMessage('last name required')

],login.register) 

Routes.post('/insert',[
	body('username').not().isEmpty().withMessage('username required'),
	body('password').not().isEmpty().withMessage('password required'),
	body('first_name').not().isEmpty().withMessage('first name required'),
	body('last_name').not().isEmpty().withMessage('last name required')

],jwt.validate,attd.attendance)

Routes.get('/info',(req, res)=>{
	res.status(200).json({
	 error : null,
	 status :[{
		 server : "OK",
		}]
	})
})

module.exports = Routes

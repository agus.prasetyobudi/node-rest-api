'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_details = sequelize.define('user_details', {
    user_id: DataTypes.INTEGER,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    position: DataTypes.STRING
  }, {});
  user_details.associate = function(models) {
    // associations can be defined here
  };
  return user_details;
};